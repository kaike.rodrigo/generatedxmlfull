package br.com.kr.tools.draw;

import br.com.kr.model.MxCell;
import br.com.kr.model.MxGeometry;
import lombok.NoArgsConstructor;

import static br.com.kr.util.constant.VariableDraw.*;

@NoArgsConstructor
public class SetPositionDraw {
    public CreateMxGeometry createMxGeometry = new CreateMxGeometry();

    public MxCell setPositionGeometryComponent(MxCell mxCell, Integer axisX, Integer axisSumX, Integer axisY, Integer axisSumY) throws Exception {
        try {
            if (mxCell.getMxGeometry() == null) {
                mxCell.setMxGeometry(new MxGeometry());
            }
            mxCell.setMxGeometry(createMxGeometry.createMxGeometryShape(mxCell.getBelongs(), null, GEOMETRY.getValue(), (axisX + axisSumX), (axisY + axisSumY), Integer.parseInt(GEOMETRY_WIDTH_DEFAULT.getValue()), Integer.parseInt(GEOMETRY_HEIGHT_DEFAULT.getValue())));
        } catch (Exception e) {
            throw new Exception("Error trying to set position component", e);
        }
        return mxCell;
    }

    public MxCell setPositionGeometryDecision(MxCell mxCell, Integer axisX, Integer axisSumX, Integer axisY, Integer axisSumY) throws Exception {
        try {
            if (mxCell.getMxGeometry() == null) {
                mxCell.setMxGeometry(new MxGeometry());
            }
            mxCell.setMxGeometry(createMxGeometry.createMxGeometryShape(mxCell.getBelongs(), null, GEOMETRY.getValue(), (axisX + axisSumX), (axisY + axisSumY), Integer.parseInt(GEOMETRY_DECISION_WIDTH_DEFAULT.getValue()), Integer.parseInt(GEOMETRY_DECISION_HEIGHT_DEFAULT.getValue())));
        } catch (Exception e) {
            throw new Exception("Error trying to set position Decision", e);
        }
        return mxCell;
    }

    public MxCell setPositionGeometryDataMessage(MxCell mxCell, Integer axisX, Integer axisSumX, Integer axisY, Integer axisSumY) throws Exception {
        try {
            if (mxCell.getMxGeometry() == null) {
                mxCell.setMxGeometry(new MxGeometry());
            }
            mxCell.setMxGeometry(createMxGeometry.createMxGeometryShape(mxCell.getBelongs(), null, GEOMETRY.getValue(), (axisX + axisSumX), (axisY + axisSumY), Integer.parseInt(GEOMETRY_WIDTH_DEFAULT.getValue()), Integer.parseInt(GEOMETRY_HEIGHT_DEFAULT.getValue())));
        } catch (Exception e) {
            throw new Exception("Error trying to set position decision", e);
        }
        return mxCell;
    }

    public MxCell setPositionGeometryCircle(MxCell mxCell, Integer axisX, Integer axisSumX, Integer axisY, Integer axisSumY) throws Exception {
        try {
            if (mxCell.getMxGeometry() == null) {
                mxCell.setMxGeometry(new MxGeometry());
            }
            mxCell.setMxGeometry(createMxGeometry.createMxGeometryShape(mxCell.getBelongs(), 1, GEOMETRY.getValue(), (axisX != null) ? (axisX + axisSumX) : Integer.parseInt(GEOMETRY_CIRCLE_X_DEFAULT.getValue()), (axisY != null) ? (axisY + axisSumY) : Integer.parseInt(GEOMETRY_CIRCLE_Y_DEFAULT.getValue()), Integer.parseInt(GEOMETRY_CIRCLE_WIDTH_DEFAULT.getValue()), Integer.parseInt(GEOMETRY_CIRCLE_HEIGHT_DEFAULT.getValue())));
        } catch (Exception e) {
            throw new Exception("Error trying to set position circle", e);
        }
        return mxCell;
    }

    public MxCell setPositionGeometryArrowConnection(MxCell mxCell, Integer axisX, Integer axisSumX, Integer axisY, Integer axisSumY) throws Exception {
        try {
            if (mxCell.getMxGeometry() == null) {
                mxCell.setMxGeometry(new MxGeometry());
            }
            mxCell.setMxGeometry(createMxGeometry.createMxGeometryShape(null, 1, GEOMETRY.getValue(), (axisX + axisSumX), (axisY + axisSumY), null, null));
        } catch (Exception e) {
            throw new Exception("Error trying to set position arrowConnection", e);
        }
        return mxCell;
    }
}
