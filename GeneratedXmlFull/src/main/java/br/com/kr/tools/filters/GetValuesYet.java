package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

import static br.com.kr.util.constant.InfluxPipelineKey.*;
import static br.com.kr.util.constant.InfluxPipelineKey.ASYNC_NEXT;

@NoArgsConstructor
public class GetValuesYet {
    private GetValuesAsyncNext getValuesAsyncNext = new GetValuesAsyncNext();

    public HashMap<String, Object> recoverValueYet(HashMap<String, Object> map) throws Exception {
        try {
            HashMap<String, Object> mapAux;
            HashMap<String, Object> objectHashMap = new HashMap<>();
            if (map.get(SEQUENCE.getValue()) != null && !map.get(SEQUENCE.getValue()).toString().isBlank()) {
                mapAux = (HashMap<String, Object>) map.get(SEQUENCE.getValue());
                if (mapAux.get(SWITCH.getValue()) != null && !mapAux.get(SWITCH.getValue()).toString().isBlank()) {
                    List<HashMap<String, Object>> hashMapList = (List<HashMap<String, Object>>) mapAux.get(SWITCH.getValue());
                    int j = 0;
                    for (int i = 0; i < hashMapList.size(); i++) {
                        if (hashMapList.get(i).get(YET.getValue()) != null && !hashMapList.get(i).get(YET.getValue()).toString().isBlank()) {
                            HashMap<String, Object> objectHashMapAux = (HashMap<String, Object>) hashMapList.get(i).get(YET.getValue());
                            if (objectHashMapAux.get(ASYNC_NEXT.getValue()) != null && !objectHashMapAux.get(ASYNC_NEXT.getValue()).toString().isBlank()) {
                                objectHashMap.put(j + "." + ASYNC_NEXT.getValue(), objectHashMapAux.get(ASYNC_NEXT.getValue()));
                            }
                            if (objectHashMapAux.get(NEXT.getValue()) != null && !objectHashMapAux.get(NEXT.getValue()).toString().isBlank()) {
                                objectHashMap.put(j + "", objectHashMapAux.get(NEXT.getValue()));
                            }
                            j++;
                        }
                    }
                }
            }
            return objectHashMap;
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve information from yet", e);
        }
    }
}
