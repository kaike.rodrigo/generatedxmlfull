package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;

@NoArgsConstructor
public class GetValuesConfig {
    private final GetValuesContext getValuesContext = new GetValuesContext();
    public Map<String, Object> getConfig(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(CODE.getValue()) != null && !mapValues.get(CODE.getValue()).toString().isBlank()) {
                objectMap.put(CODE.getValue(), mapValues.get(CODE.getValue()).toString());
            }
            if (mapValues.get(JS_TRANSFORM.getValue()) != null && !mapValues.get(JS_TRANSFORM.getValue()).toString().isBlank()) {
                objectMap.put(JS_TRANSFORM.getValue(), mapValues.get(JS_TRANSFORM.getValue()).toString());
            }
            if (mapValues.get(OUTPUT_FIELD.getValue()) != null && !mapValues.get(OUTPUT_FIELD.getValue()).toString().isBlank()) {
                objectMap.put(OUTPUT_FIELD.getValue(), mapValues.get(OUTPUT_FIELD.getValue()).toString());
            }
            if (mapValues.get(OUTPUT_FIELD_CODE.getValue()) != null && !mapValues.get(OUTPUT_FIELD_CODE.getValue()).toString().isBlank()) {
                objectMap.put(OUTPUT_FIELD_CODE.getValue(), mapValues.get(OUTPUT_FIELD_CODE.getValue()).toString());
            }
            if (mapValues.get(CONTEXT.getValue()) != null && !mapValues.get(CONTEXT.getValue()).toString().isBlank()) {
                objectMap.put(CONTEXT.getValue(), getValuesContext.getContext((Map<String, Object>) mapValues.get(CONTEXT.getValue())));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value config: ", e);
        }
        return objectMap;
    }
}
