package br.com.kr.tools.draw;

import br.com.kr.model.MxGeometry;

import static br.com.kr.util.constant.VariableDraw.GEOMETRY;


public class CreateMxGeometry {
        public MxGeometry createMxGeometryShape(String belongs, Integer relative, String as, Integer x, Integer y, Integer width, Integer height) {
        MxGeometry mxGeometry = new MxGeometry();
        if (belongs != null && !belongs.isEmpty() && !belongs.isBlank()) {
            mxGeometry.setBelongs(belongs);
        }
        if (as != null && !as.isEmpty() && !as.isBlank()) {
            mxGeometry.setAs(as);
        } else {
            mxGeometry.setAs(GEOMETRY.getValue());
        }
        if (x != null) {
            mxGeometry.setX(x);
        }
        if (y != null) {
            mxGeometry.setY(y);
        }
        if (width != null) {
            mxGeometry.setWidth(width);
        }
        if (height != null) {
            mxGeometry.setHeight(height);
        }
        if (relative != null) {
            mxGeometry.setRelative(relative);
        }
        return mxGeometry;
    }
}
