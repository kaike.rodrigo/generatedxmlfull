package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;

@NoArgsConstructor
public class GetValuesContext {

    private final GetValuesAdd getValuesAdd = new GetValuesAdd();
    private final GetValuesAfter getValuesAfter = new GetValuesAfter();
    private final GetValuesBefore getValuesBefore = new GetValuesBefore();

    public Map<String, Object> getContext(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(ADD.getValue()) != null && !mapValues.get(ADD.getValue()).toString().isBlank()) {
                objectMap.put(ADD.getValue(), getValuesAdd.getAdd((Map<String, Object>) mapValues.get(ADD.getValue())));
                objectMap.put(ADD_DATA.getValue(), mapValues.get(ADD.getValue()));
            }
            if (mapValues.get(AFTER.getValue()) != null && !mapValues.get(AFTER.getValue()).toString().isBlank()) {
                objectMap.put(AFTER.getValue(), getValuesAfter.getAfter((Map<String, Object>) mapValues.get(AFTER.getValue())));
            }
            if (mapValues.get(BEFORE.getValue()) != null && !mapValues.get(BEFORE.getValue()).toString().isBlank()) {
                objectMap.put(BEFORE.getValue(), getValuesBefore.getBefore((Map<String, Object>) mapValues.get(BEFORE.getValue())));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value context: ", e);
        }
        return objectMap;
    }
}
