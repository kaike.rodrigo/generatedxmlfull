package br.com.kr.tools.filters;

import br.com.kr.util.constant.InfluxPipelineKey;
import lombok.NoArgsConstructor;

import java.util.*;

import static br.com.kr.util.constant.InfluxPipelineKey.*;


@NoArgsConstructor
public class GetValuesAsyncNext {
    public HashMap<String, List<String>> recoverValueAsync(HashMap<String, Object> map) throws Exception {
        try {
            HashMap<String, Object> mapAux;
            HashMap<String, List<String>> mapString = new HashMap<>();
            if (map.get(SEQUENCE.getValue()) != null && !map.get(SEQUENCE.getValue()).toString().isBlank()) {
                mapAux = (HashMap<String, Object>) map.get(SEQUENCE.getValue());
                if (mapAux.get(SWITCH.getValue()) != null && !mapAux.get(SWITCH.getValue()).toString().isBlank()) {
                    List<HashMap<String, Object>> hashMapList = (List<HashMap<String, Object>>) mapAux.get(SWITCH.getValue());
                    int j = 0;
                    for (int i = 0; i < hashMapList.size(); i++) {
                        if (hashMapList.get(i).get(ASYNC_NEXT.getValue()) != null && !hashMapList.get(i).get(ASYNC_NEXT.getValue()).toString().isBlank()) {
                            mapString.put(j + "", (List<String>) hashMapList.get(i).get(ASYNC_NEXT.getValue()));
                            j++;
                        }
                    }
                }
            }
            return mapString;
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve information from async", e);
        }
    }
}
