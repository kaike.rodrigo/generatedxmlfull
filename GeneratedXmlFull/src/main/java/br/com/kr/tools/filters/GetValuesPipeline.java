package br.com.kr.tools.filters;


import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;


@NoArgsConstructor
public class GetValuesPipeline {
    private GetValuesStep getValuesStep = new GetValuesStep();
    public Map<String, Object> getDetailPipeline(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(VERSION.getValue()) != null && !mapValues.get(VERSION.getValue()).toString().isBlank()) {
                objectMap.put(VERSION.getValue(), mapValues.get(VERSION.getValue()).toString());
            }
            if (mapValues.get(NAME.getValue()) != null && !mapValues.get(NAME.getValue()).toString().isBlank()) {
                objectMap.put(NAME.getValue(), mapValues.get(NAME.getValue()).toString());
            }
            if (mapValues.get(TYPE.getValue()) != null && !mapValues.get(TYPE.getValue()).toString().isBlank()) {
                objectMap.put(TYPE.getValue(), mapValues.get(TYPE.getValue()).toString());
            }
            if (mapValues.get(STEPS.getValue()) != null && !mapValues.get(STEPS.getValue()).toString().isBlank()) {
                objectMap.put(STEPS.getValue(), getValuesStep.getSteps(mapValues));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value pipeline: ", e);
        }
        return objectMap;
    }

}
