package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;

@NoArgsConstructor
public class GetValuesStep {
    private final GetValuesConfig getValuesConfig = new GetValuesConfig();
    private final GetValueSequence getValueSequence = new GetValueSequence();

    public Map<String, Object> getSteps(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        ArrayList stepObject = (ArrayList) mapValues.get(STEPS.getValue());
        try {
            for (int i = 0; i < stepObject.size(); i++) {
                Map<String, Object> step = new HashMap<>();
                Map<String, Object> stepMap = (Map<String, Object>) stepObject.get(i);
                if (stepMap.get(STEP.getValue()) != null && !stepMap.get(STEP.getValue()).toString().isBlank()) {
                    step.put(STEP.getValue(), stepMap.get(STEP.getValue()).toString());
                }
                if (stepMap.get(DESCRIPTION.getValue()) != null && !stepMap.get(DESCRIPTION.getValue()).toString().isBlank()) {
                    step.put(DESCRIPTION.getValue(), stepMap.get(DESCRIPTION.getValue()).toString());
                }
                if (stepMap.get(COMPONENT.getValue()) != null && !stepMap.get(COMPONENT.getValue()).toString().isBlank()) {
                    step.put(COMPONENT.getValue(), stepMap.get(COMPONENT.getValue()).toString());
                }
                if (stepMap.get(MODE.getValue()) != null && !stepMap.get(MODE.getValue()).toString().isBlank()) {
                    step.put(MODE.getValue(), stepMap.get(MODE.getValue()).toString());
                }
                if (stepMap.get(TYPE.getValue()) != null && !stepMap.get(TYPE.getValue()).toString().isBlank()) {
                    step.put(TYPE.getValue(), stepMap.get(TYPE.getValue()).toString());
                }
                if (stepMap.get(DESTINATION.getValue()) != null && !stepMap.get(DESTINATION.getValue()).toString().isBlank()) {
                    step.put(DESTINATION.getValue(), stepMap.get(DESTINATION.getValue()).toString());
                }
                if (stepMap.get(CONFIG.getValue()) != null && !stepMap.get(CONFIG.getValue()).toString().isBlank()) {
                    step.put(CONFIG.getValue(), getValuesConfig.getConfig((Map<String, Object>) stepMap.get(CONFIG.getValue())));
                }
                if (stepMap.get(SEQUENCE.getValue()) != null && !stepMap.get(SEQUENCE.getValue()).toString().isBlank()) {
                    if (stepMap.get(SEQUENCE.getValue()) instanceof Map<?, ?>) {
                        step.put(SEQUENCE.getValue(), getValueSequence.getSequence((Map<String, Object>) stepMap.get(SEQUENCE.getValue())));
                    } else {
                        step.put(SEQUENCE.getValue(), getValueSequence.getSequenceArrayListObject((Map<String, Object>) stepMap.get(SEQUENCE.getValue())));
                    }
                }
                objectMap.put(i + "", step);
            }
            return objectMap;
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value steps: ", e);
        }
    }
}
