package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;


@NoArgsConstructor
public class GetValuesAdd {

    public Map<String, Object> getAdd(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMapAdd = new HashMap<>();
        try {
            if (mapValues.get(MESSAGE_CODE.getValue()) != null && !mapValues.get(MESSAGE_CODE.getValue()).toString().isBlank()) {
                objectMapAdd.put(MESSAGE_CODE.getValue(), mapValues.get(MESSAGE_CODE.getValue()).toString());
            }
            if (mapValues.get(MESSAGE_DETAIL.getValue()) != null && !mapValues.get(MESSAGE_DETAIL.getValue()).toString().isBlank()) {
                objectMapAdd.put(MESSAGE_DETAIL.getValue(), mapValues.get(MESSAGE_DETAIL.getValue()).toString());
            }
            if (mapValues.get(MESSAGE_BUSINESS.getValue()) != null && !mapValues.get(MESSAGE_BUSINESS.getValue()).toString().isBlank()) {
                objectMapAdd.put(MESSAGE_BUSINESS.getValue(), mapValues.get(MESSAGE_BUSINESS.getValue()).toString());
            }
            if (mapValues.get(MESSAGE_TITLE.getValue()) != null && !mapValues.get(MESSAGE_TITLE.getValue()).toString().isBlank()) {
                objectMapAdd.put(MESSAGE_TITLE.getValue(), mapValues.get(MESSAGE_TITLE.getValue()).toString());
            }
            if (objectMapAdd.isEmpty()) {
                if (mapValues.get(ADD_DATA.getValue()) != null && !mapValues.get(ADD_DATA.getValue()).toString().isBlank()) {
                    objectMapAdd.put(ADD_DATA.getValue(), mapValues.toString());
                }
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value add: ", e);
        }
        return objectMapAdd;
    }
}
