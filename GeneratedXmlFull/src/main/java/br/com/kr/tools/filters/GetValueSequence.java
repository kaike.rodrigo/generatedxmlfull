package br.com.kr.tools.filters;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;

@NoArgsConstructor
public class GetValueSequence {
    public Map<String, Object> getSequenceArrayListObject(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(NEXT.getValue()) != null && !mapValues.get(NEXT.getValue()).toString().isBlank()) {
                objectMap.put(NEXT.getValue(), mapValues.get(NEXT.getValue()));
            } else if (mapValues.get(SWITCH.getValue()) != null && !mapValues.get(SWITCH.getValue()).toString().isBlank()) {
                objectMap.put(SWITCH.getValue(), mapValues.get(SWITCH.getValue()));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value sequence: ", e);
        }
        return objectMap;
    }

    public Map<String, Object> getSequence(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(SWITCH.getValue()) != null && !mapValues.get(SWITCH.getValue()).toString().isBlank()) {
                objectMap.put(SWITCH.getValue(), mapValues.get(SWITCH.getValue()));
            }
            if (mapValues.get(NEXT.getValue()) != null && !mapValues.get(NEXT.getValue()).toString().isBlank()) {
                objectMap.put(NEXT.getValue(), mapValues.get(NEXT.getValue()));
            }
            if (mapValues.get(SEQUENCE.getValue()) != null && !mapValues.get(SEQUENCE.getValue()).toString().isBlank()) {
                objectMap.put(SEQUENCE.getValue(), mapValues.get(SEQUENCE.getValue()));
            }
            if (mapValues.get(ASYNC_NEXT.getValue()) != null && !mapValues.get(ASYNC_NEXT.getValue()).toString().isBlank()) {
                objectMap.put(ASYNC_NEXT.getValue(), mapValues.get(ASYNC_NEXT.getValue()));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value sequence: ", e);
        }
        return objectMap;
    }
}
