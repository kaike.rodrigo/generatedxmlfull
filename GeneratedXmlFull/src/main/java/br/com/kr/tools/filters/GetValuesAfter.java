package br.com.kr.tools.filters;


import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.ADD;
import static br.com.kr.util.constant.InfluxPipelineKey.ADD_DATA;

@NoArgsConstructor
public class GetValuesAfter {
    private final GetValuesAdd getValuesAdd = new GetValuesAdd();
    public Map<String, Object> getAfter(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        try {
            if (mapValues.get(ADD.getValue()) != null && !mapValues.get(ADD.getValue()).toString().isBlank()) {
                objectMap.put(ADD.getValue(), getValuesAdd.getAdd((Map<String, Object>) mapValues.get(ADD.getValue())));
                objectMap.put(ADD_DATA.getValue(), mapValues.get(ADD.getValue()));
            }
        } catch (Exception e) {
            throw new Exception("Error trying to retrieve value after: ", e);
        }
        return objectMap;
    }
}
