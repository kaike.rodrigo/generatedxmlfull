package br.com.kr.tools.draw;

import br.com.kr.model.MxCell;
import br.com.kr.model.MxGeometry;
import br.com.kr.util.generated.GeneratedRandomId;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static br.com.kr.util.constant.InfluxPipelineKey.*;
import static br.com.kr.util.constant.VariableDraw.*;
import static br.com.kr.util.constant.VariableStyleDraw.COLOR_GREEN_DEFAULT;
import static br.com.kr.util.constant.VariableStyleDraw.COLOR_WHITE_DEFAULT;
import static br.com.kr.util.constant.VariableTypeObject.DECISION;

@NoArgsConstructor
public class CreateDrawShape {

    private final GeneratedRandomId generatedRandomId = new GeneratedRandomId();

    public List<MxCell> startDrawShapeDefault() throws Exception {
        List<MxCell> mxCellList = new ArrayList<>();
        MxCell mxCell = new MxCell();
        String firstId = ("firstCell-" + generatedRandomId.randomInt().replaceAll("-", ""));
        String secondId = ("secondCell-" + generatedRandomId.randomInt().replaceAll("-", ""));
        mxCell.setId(firstId);
        mxCell.setStep("1");
        mxCellList.add(mxCell);
        mxCell = new MxCell();
        mxCell.setStep("2");
        mxCell.setParent(firstId);
        mxCell.setId(secondId);
        mxCellList.add(mxCell);
        mxCell = new MxCell();
        createDraw(
                mxCell,
                null,
                secondId,
                secondId,
                null,
                null,
                null,
                null,
                "3",
                COLOR_GREEN_DEFAULT.getValue(),
                null,
                null,
                null,
                CIRCLE_DRAW.getValue(),
                true
        );
        mxCell.setMxGeometry(new MxGeometry());
        mxCell.getMxGeometry().setX(Integer.parseInt(GEOMETRY_CIRCLE_X_DEFAULT.getValue()));
        mxCell.getMxGeometry().setY(Integer.parseInt(GEOMETRY_CIRCLE_Y_DEFAULT.getValue()));
        mxCell.getMxGeometry().setWidth(Integer.parseInt(GEOMETRY_CIRCLE_WIDTH_DEFAULT.getValue()));
        mxCell.getMxGeometry().setHeight(Integer.parseInt(GEOMETRY_CIRCLE_HEIGHT_DEFAULT.getValue()));
        mxCell.getMxGeometry().setAs(GEOMETRY.getValue());
        mxCellList.add(mxCell);
        return mxCellList;
    }

    public void createDraw(
            MxCell mxCell,
            String id,
            String belongs,
            String parent,
            String source,
            String style,
            String target,
            String value,
            String step,
            String backgroundColor,
            String isPackOrBundle,
            String typeComponent,
            String descriptionComponent,
            String typeDraw,
            Boolean start
    ) throws Exception {
        try {
            if (typeDraw != null) {
                if (parent != null && !parent.isEmpty()) {
                    mxCell.setParent(parent);
                }
                if (belongs != null && !belongs.isEmpty()) {
                    mxCell.setBelongs(belongs);
                }
                if (value != null && !value.isEmpty()) {
                    mxCell.setValue(value);
                } else if (typeDraw.equals(CIRCLE_DRAW.getValue())) {
                    mxCell.setValue(((start) ? "Start" : "Finish"));
                }
                if (source != null && !source.isEmpty() && !source.isBlank()) {
                    mxCell.setSource(source);
                }
                if (target != null && !target.isEmpty() && !target.isBlank()) {
                    mxCell.setTarget(target);
                }
                if (parent != null && !parent.isEmpty() && !parent.isBlank()) {
                    mxCell.setParent(parent);
                }
                if (typeDraw.equals(CIRCLE_DRAW.getValue())) {
                    circleDraw(mxCell, step, backgroundColor, style, start);
                } else if (typeDraw.equals(DECISION_DRAW.getValue())) {
                    decisionDraw(mxCell, value, style, step);
                } else if (typeDraw.equals(SHAPE_BACKGROUND_DRAW.getValue())) {
                    shapeBackgroundDraw(mxCell, id, value, isPackOrBundle, typeComponent, descriptionComponent, style, backgroundColor, step);
                } else if (typeDraw.equals(CONNECTION_DRAW.getValue())) {
                    createConnection(mxCell, id, style, step);
                } else if (typeDraw.equals(CONNECTION_ASYNC_DRAW.getValue())) {
                    createConnectionAsync(mxCell, id, style, step);
                }
            } else {
                throw new Exception("No format was provided for development");
            }

        } catch (Exception e) {
            throw new Exception("Error trying to draw " + typeDraw + ":", e);
        }
    }

    public void circleDraw(
            MxCell mxCell,
            String step,
            String backgroundColor,
            String style,
            Boolean start
    ) throws Exception {
        try {
            String id = ("circle-");
            if (start != null) {
                id += ((start) ? "start" : "finish");
            }
            if (step != null && !step.isEmpty() && !step.isBlank()) {
                id += step;
            }
            mxCell.setId(id);
            if (style != null && !style.isEmpty() && !style.isBlank()) {
                mxCell.setStyle(style);
            } else {
                if (backgroundColor != null && !backgroundColor.isEmpty()) {
                    mxCell.setStyle("ellipse;whiteSpace=wrap;html=1;fillColor=" + backgroundColor + ";strokeColor=#000000;strokeWidth=1;perimeterSpacing=1;");
                } else {
                    mxCell.setStyle("ellipse;whiteSpace=wrap;html=1;fillColor=" + COLOR_WHITE_DEFAULT.getValue() + ";strokeColor=#000000;strokeWidth=1;perimeterSpacing=1;");
                }
            }
            mxCell.setVertex(1);
        } catch (Exception e) {
            throw new Exception("Error trying to draw circle:", e);
        }
    }

    public void decisionDraw(
            MxCell mxCell,
            String value,
            String style,
            String step
    ) throws Exception {
        try {
            String id = ("decision-" + generatedRandomId.randomInt().replaceAll("-", "") + "-");
            if (step != null && !step.isEmpty() && !step.isBlank()) {
                mxCell.setStep(step);
                id += step;
            }
            mxCell.setId(id);
            if (value != null && !value.isEmpty()) {
                mxCell.setValue(value);
            } else {
                mxCell.setValue("Decision&lt;br&gt;&lt;br&gt;&lt;div&gt;"+step+"&lt;br&gt;&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;");
            }
            if (style != null && !style.isEmpty() && !style.isBlank()) {
                mxCell.setStyle(style);
            } else {
                mxCell.setStyle("rhombus;whiteSpace=wrap;html=1;");
            }
            mxCell.setVertex(1);
        } catch (Exception e) {
            throw new Exception("Error trying to draw decision:", e);
        }
    }

    public void shapeBackgroundDraw(
            MxCell mxCell,
            String id,
            String value,
            String isPackOrBundle,
            String typeComponent,
            String descriptionComponent,
            String style,
            String backgroundColor,
            String step
    ) throws Exception {
        try {
            String idAux;
            if (id != null && !id.isEmpty()) {
                idAux = generatedRandomId.randomInt().replaceAll("-", "") + "-" + id;
            } else {
                idAux = ("shapeBackground-" + generatedRandomId.randomInt().replaceAll("-", "") + "-");
            }
            if (step != null && !step.isEmpty() && !step.isBlank()) {
                mxCell.setStep(step);
                idAux += step;
            }
            mxCell.setId(idAux);
            if (isPackOrBundle == null || isPackOrBundle.isBlank()) {
                isPackOrBundle = "";
            }
            if (typeComponent == null || typeComponent.isBlank()) {
                typeComponent = "";
            }
            if (descriptionComponent == null || descriptionComponent.isBlank()) {
                descriptionComponent = "";
            }
            if (value != null && !value.isEmpty()) {
                mxCell.setValue(value);
            } else {
                mxCell.setValue("&lt;div style=&quot;text-align: justify;&quot;&gt;&lt;b style=&quot;font-size: 10px; background-color:" + backgroundColor + ";&quot;&gt;&lt;span style=&quot;&quot;&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;&quot;&gt;                                                                &lt;/span&gt;&lt;span style=&quot;&quot;&gt; &lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;&lt;span style=&quot;white-space: pre;&quot;&gt;\t&lt;/span&gt;" + isPackOrBundle + "&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;&lt;font style=&quot;font-size: 10px&quot;&gt;&lt;b&gt;" + typeComponent + "&lt;/b&gt;&lt;/font&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;&lt;font style=&quot;font-size: 10px&quot;&gt;&lt;br&gt;&lt;/font&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;" + descriptionComponent + "&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;" + step + "&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;&quot;&gt;&lt;br&gt;&lt;/div&gt;");
            }
            if (style != null && !style.isEmpty() && !style.isBlank()) {
                mxCell.setStyle(style);
            } else {
                if (backgroundColor != null && !backgroundColor.isEmpty()) {
                    mxCell.setStyle("whiteSpace=wrap;html=1;fillColor=" + backgroundColor + ";strokeColor=#6c8ebf;");
                } else {
                    mxCell.setStyle("whiteSpace=wrap;html=1;fillColor=" + COLOR_WHITE_DEFAULT.getValue() + ";strokeColor=#6c8ebf;");
                }
            }
            mxCell.setVertex(1);
        } catch (Exception e) {
            throw new Exception("Error trying to draw decision:", e);
        }
    }

    public void createConnection(
            MxCell mxCell,
            String id,
            String style,
            String step
    ) throws Exception {
        try {
            if (id != null && !id.isEmpty()) {
                mxCell.setId(generatedRandomId.randomInt().replaceAll("-", "") + "-" + id);
            } else {
                mxCell.setId(("arrowConnection-" + generatedRandomId.randomInt().replaceAll("-", "") + "-next_" + step));
            }
            if (step != null && !step.isEmpty() && !step.isBlank()) {
                mxCell.setStep(step);
            }
            if (style != null && !style.isEmpty() && !style.isBlank()) {
                mxCell.setStyle(style);
            } else {
                mxCell.setStyle("edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;");
            }
            mxCell.setEdge(1);
        } catch (Exception e) {
            throw new Exception("Error trying to draw connection:", e);
        }
    }

    public void createConnectionAsync(
            MxCell mxCell,
            String id,
            String style,
            String step
    ) throws Exception {
        try {
            if (id != null && !id.isEmpty()) {
                mxCell.setId(id);
            } else {
                mxCell.setId(("arrowConnection-" + generatedRandomId.randomInt().replaceAll("-", "") + "-async_" + step));
            }
            if (step != null && !step.isEmpty() && !step.isBlank()) {
                mxCell.setStep(step);
            }
            if (style != null && !style.isEmpty() && !style.isBlank()) {
                mxCell.setStyle(style);
            } else {
                mxCell.setStyle("edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;dashed=1;");
            }
            mxCell.setEdge(1);
        } catch (Exception e) {
            throw new Exception("Error trying to draw connection async:", e);
        }
    }

    public String getGlobalConfigCodeStepValue(Map<String, Object> mapValues) throws Exception {
        Map<String, Object> objectMap = new HashMap<>();
        String value = null;
        try {
            if (mapValues.get(CONFIG.getValue()) != null && !mapValues.get(CONFIG.getValue()).toString().isBlank()) {
                objectMap.put(CONFIG.getValue(), mapValues.get(CONFIG.getValue()).toString());
                if (objectMap.get(CODE.getValue()) != null && !mapValues.get(CODE.getValue()).toString().isBlank()) {
                    objectMap.put(CODE.getValue(), objectMap.get(CODE.getValue()));
                    value = String.valueOf(objectMap.get(CODE.getValue()));
                } else {
                    value = String.valueOf(objectMap.get(CONFIG.getValue()));
                }
                if (value.contains("=") && value.split("=").length > 0) {
                    value = value.split("=")[1].replaceAll("}", ">");
                    if (value.contains("TSMC_BANKING_SYSTEM")) {
                        value = value.split(">")[1];
                        value = "TSMC" + value;
                    }
                    value = value.replaceAll(">", "");
                }
            }
        } catch (Exception e) {
            throw new Exception("Error global - trying to retrieve value config code: ", e);
        }
        return ((value != null && value.equals("apiOutput")) ? mapValues.get(DESCRIPTION.getValue()).toString() : value);
    }
}
