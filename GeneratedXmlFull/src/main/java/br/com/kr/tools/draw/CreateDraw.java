package br.com.kr.tools.draw;

import br.com.kr.model.MxCell;
import br.com.kr.model.Root;
import br.com.kr.tools.filters.GetValuesAsyncNext;
import br.com.kr.tools.filters.GetValuesPipeline;
import br.com.kr.tools.filters.GetValuesYet;
import br.com.kr.util.constant.InfluxPipelineKey;
import br.com.kr.util.constant.VariableDraw;
import br.com.kr.util.constant.VariableStyleDraw;
import br.com.kr.util.constant.VariableTypeObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static br.com.kr.util.constant.VariableDraw.DECISION_DRAW;
import static br.com.kr.util.constant.VariableDraw.SHAPE_BACKGROUND_DRAW;

@NoArgsConstructor
public class CreateDraw {
    private CreateDrawShape createDrawShape = new CreateDrawShape();
    //    private GetValuesAsyncNext getValuesAsyncNext = new GetValuesAsyncNext();
//    private GetValuesYet getValuesYet = new GetValuesYet();
    private GetValuesPipeline getValuesPipeline = new GetValuesPipeline();
    private SetPositionDraw setPositionDraw = new SetPositionDraw();


    public String createDrawing(String jsonPipeline) throws Exception {
        List<MxCell> mxCellList = createDrawShape.startDrawShapeDefault();
        Map<String, Object> mapYet = new HashMap<>();
        List<MxCell> mxCellsAudit;
        List<MxCell> mxCellsAsync;
        Root root = new Root();
        try {
            Map<String, Object> pipelineMap = getValuesPipeline.getDetailPipeline(new ObjectMapper().readValue(jsonPipeline, new TypeReference<>() {
            }));
            Map<String, Object> steps = (Map<String, Object>) pipelineMap.get(InfluxPipelineKey.STEPS.getValue());
            for (int i = 0; i <= steps.size(); i++) {
                if (steps.get(String.valueOf(i)) != null) {
                    MxCell mxCell = new MxCell();
                    HashMap<String, Object> infoStep = (HashMap<String, Object>) steps.get(String.valueOf(i));
                    String pipelineComponentValue = infoStep.get(InfluxPipelineKey.COMPONENT.getValue()).toString();
                    boolean isDecision = pipelineComponentValue != null && !pipelineComponentValue.isBlank() && (pipelineComponentValue.contains(VariableTypeObject.DECISION.getValue()));
                    mxCell.setStep(infoStep.get(InfluxPipelineKey.STEP.getValue()).toString());
                    mxCell.setBelongs(mxCellList.get(1).getId());
                    mxCell.setParent(mxCellList.get(1).getId());
//                    mxCell.setAsyncLinks(getValuesAsyncNext.recoverValueAsync(infoStep));
                    if (isDecision) {
                        drawDecision(mxCell, infoStep);
                    } else {
                        drawBackgroundShape(mxCell, infoStep);
                    }
//                    if(getValuesYet.recoverValueYet(infoStep).size() > 0){
//                        mapYet.put(infoStep.get(InfluxPipelineKey.STEP.getValue()).toString(), getValuesYet.recoverValueYet(infoStep));
//                    }
                    mxCellList.add(mxCell);
                }
            }
//            mxCellsAudit = separateAudits(mxCellList);
            root.setMxCell(setPosition(mxCellList));

        } catch (Exception e) {
            throw new Exception("Error trying to create draw:", e);
        }

        return root.toString();
    }

    public void drawBackgroundShape(MxCell mxCell, Map<String, Object> map) throws Exception {
        try {
            String backgroundColor = VariableStyleDraw.COLOR_WHITE_DEFAULT.getValue();
            if (map.get(InfluxPipelineKey.COMPONENT.getValue()).toString().contains("pipeline")) {
                backgroundColor = VariableStyleDraw.COLOR_BLUE_DEFAULT.getValue();
            }
            createDrawShape.createDraw(
                    mxCell,
                    null,
                    mxCell.getBelongs(),
                    mxCell.getParent(),
                    null,
                    null,
                    null,
                    null,
                    mxCell.getStep(),
                    backgroundColor,
                    null,
                    map.get(InfluxPipelineKey.COMPONENT.getValue()).toString(),
                    map.get(InfluxPipelineKey.DESCRIPTION.getValue()).toString(),
                    SHAPE_BACKGROUND_DRAW.getValue(),
                    null
            );
        } catch (Exception e) {
            throw new Exception("Error trying to create draw background shape:", e);
        }
    }

    public void drawDecision(MxCell mxCell, Map<String, Object> map) throws Exception {
        try {
            String backgroundColor = VariableStyleDraw.COLOR_WHITE_DEFAULT.getValue();
            createDrawShape.createDraw(
                    mxCell,
                    null,
                    mxCell.getBelongs(),
                    mxCell.getParent(),
                    null,
                    null,
                    null,
                    null,
                    mxCell.getStep(),
                    backgroundColor,
                    null,
                    map.get(InfluxPipelineKey.COMPONENT.getValue()).toString(),
                    map.get(InfluxPipelineKey.DESCRIPTION.getValue()).toString(),
                    DECISION_DRAW.getValue(),
                    null
            );
        } catch (Exception e) {
            throw new Exception("Error trying to create draw background shape:", e);
        }
    }

    public List<MxCell> separateAudits(List<MxCell> mxCellList) throws Exception {
        try {
            List<MxCell> listAudits = new ArrayList<>();
            for (MxCell mxCell : mxCellList) {
                if (mxCell.getId().contains("AUDIT")) {
                    listAudits.add(mxCell);
                }
            }
            return listAudits;
        } catch (Exception e) {
            throw new Exception("Error trying to create draw background shape:", e);
        }
    }

    private List<MxCell> setPosition(List<MxCell> mxCells) throws Exception {
        List<MxCell> listSettingPosition = new ArrayList<>();
        int dxValueCurrent = 0;
        int dyValueCurrent = 0;
        int dxRightValueCurrent = 0;
        int dyRightValueCurrent = 0;
        int dxLeftValueCurrent = 0;
        int dyLeftValueCurrent = 0;
        try {
            dxValueCurrent = 1225;
            dyValueCurrent = 300;
            for (int i = 0; i < mxCells.size(); i++) {
                if (i <= 2) {
                    listSettingPosition.add(mxCells.get(i));
                } else {
                    if (mxCells.get(i).getStep().contains(VariableTypeObject.DECISION.getValue())) {
                        listSettingPosition.add(setPositionDraw.setPositionGeometryDecision(mxCells.get(i), dxValueCurrent, 0, dyValueCurrent, 0));
                    } else {
                        listSettingPosition.add(setPositionDraw.setPositionGeometryComponent(mxCells.get(i), dxValueCurrent, 0, dyValueCurrent, 0));
                    }
                }
                dyValueCurrent += 150;
            }
        } catch (Exception e) {
            throw new Exception("Error trying to setting position:", e);
        }
        return listSettingPosition;
    }
}
