package br.com.kr.model;

import lombok.*;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class XArray {
    private String as;
    private List<MxPoint> mxPoint;
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("<Array as=\"points\">");
        for(MxPoint mxPoint1: mxPoint){
            sb.append(mxPoint1);
        }
        sb.append("</Array>");
        return sb.toString();
    }
}
