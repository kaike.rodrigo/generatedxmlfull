package br.com.kr.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MxPoint {
    private Integer x;
    private Integer y;
    private String as;
    public MxPoint(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }
    public String toString() {
        final StringBuffer sb = new StringBuffer("<mxPoint");
        sb.append(" x=\"").append(x).append("\"");
        sb.append(" y=\"").append(y).append("\"");
        sb.append("/>");
        return sb.toString();
    }
}
