package br.com.kr.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserObject {
    private String label;
    private String id;
    public List<MxCell> mxCell;
    public List<UserObject> UserObject;
    private String belongs;
    private Integer step;
}
