package br.com.kr.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Root {
    public List<MxCell> mxCell;
    public List<UserObject> userObject;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("<mxGraphModel dx=\"2450\" dy=\"423\" grid=\"1\" gridSize=\"10\" guides=\"1\" tooltips=\"1\" connect=\"1\" arrows=\"1\" fold=\"1\" page=\"0\" pageScale=\"1\" pageWidth=\"827\" pageHeight=\"1169\" math=\"0\" shadow=\"0\">");
        sb.append("<root>");
        for (MxCell cell : mxCell) {
            sb.append(cell);
        }
        sb.append("</root>");
        sb.append("</mxGraphModel>");
        return sb.toString();
    }
}
