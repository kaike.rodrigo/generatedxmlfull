package br.com.kr.model;

import lombok.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MxCell {
    private String id;
    private String parent;
    private Integer edge;
    private MxGeometry mxGeometry;
    private String style;
    private String source;
    private String target;
    private Integer vertex;
    private String value;
    private String belongs;
    private String step;
    private HashMap<String, List<String>> asyncLinks= new HashMap<>();
    private HashMap<String, List<String>> nextLinks = new HashMap<>();
    private HashMap<String, Object> yetLinks = new HashMap<>();
    private List<String> messageAsyncLinks = new ArrayList<>();
    private List<String> messageNextLinks = new ArrayList<>();
    private List<String> messageYetLinks = new ArrayList<>();
    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("<mxCell");
        if (id != null && !id.isEmpty()) {
            sb.append(" id=").append("\"").append(id).append("\"");
        }
        if (value != null && !value.isEmpty()) {
            sb.append(" value=").append("\"").append(value).append("\"");
        }
        if (style != null && !style.isEmpty()) {
            sb.append(" style=").append("\"").append(style).append("\"");
        }
        if (vertex != null) {
            sb.append(" vertex=").append("\"").append(vertex).append("\"");
        }
        if (parent != null && !parent.isEmpty()) {
            sb.append(" parent=").append("\"").append(parent).append("\"");
        }
        if (source != null && !source.isEmpty()) {
            sb.append(" source=").append("\"").append(source).append("\"");
        }
        if (target != null && !target.isEmpty()) {
            sb.append(" target=").append("\"").append(target).append("\"");
        }
        if (edge != null) {
            sb.append(" edge=").append("\"").append(edge).append("\"");
        }
        if (mxGeometry != null) {
            sb.append(">");
            sb.append(mxGeometry);
            sb.append("</mxCell>");
        } else {
            sb.append("/>");
        }
        return sb.toString();
    }
}
