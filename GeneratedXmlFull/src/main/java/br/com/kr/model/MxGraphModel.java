package br.com.kr.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MxGraphModel {
    private String id;
    private Integer relative;
    private String as;
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private XArray xArray;
    private MxPoint mxPoint;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("<mxGeometry");
        if (id != null && !id.isEmpty()) {
            sb.append(" id=").append("\"").append(id).append("\"");
        }
        if (relative == null || (relative != null && relative == 0)) {
            sb.append(" x=").append("\"").append(x).append("\"");
            sb.append(" y=").append("\"").append(y).append("\"");
            sb.append(" width=").append("\"").append(width).append("\"");
            sb.append(" height=").append("\"").append(height).append("\"");
        } else {
            sb.append(" relative=").append("\"").append(relative).append("\"");
        }
        sb.append(" as=").append("\"").append(as).append("\"");
        if (xArray != null) {
            sb.append(">");
            sb.append(xArray);
            sb.append("</mxGeometry>");
        } else {
            sb.append("/>");
        }
        return sb.toString();
    }
}
