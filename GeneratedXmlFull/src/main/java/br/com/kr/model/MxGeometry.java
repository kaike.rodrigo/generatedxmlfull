package br.com.kr.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MxGeometry {
    private String id;
    private Integer relative;
    private String as;
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private XArray xArray;
    private MxPoint mxPoint;
    private String belongs;
    private Integer step;
    private List<String> asyncLinks = new ArrayList<>();
    private List<String> nextLinks = new ArrayList<>();
    private List<String> yetLinks = new ArrayList<>();
    private List<String> messageAsyncLinks = new ArrayList<>();
    private List<String> messageNextLinks = new ArrayList<>();
    private List<String> messageYetLinks = new ArrayList<>();
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("<mxGeometry");
        if (id != null && !id.isEmpty()) {
            sb.append(" id=").append("\"").append(id).append("\"");
        }
        if (relative == null || (relative != null && relative == 0)) {
            sb.append(" x=").append("\"").append(x).append("\"");
            sb.append(" y=").append("\"").append(y).append("\"");
            sb.append(" width=").append("\"").append(width).append("\"");
            sb.append(" height=").append("\"").append(height).append("\"");
        } else {
            sb.append(" relative=").append("\"").append(relative).append("\"");
        }
        sb.append(" as=").append("\"").append(as).append("\"");
        if (xArray != null) {
            sb.append(">");
            sb.append(xArray);
            sb.append("</mxGeometry>");
        } else {
            sb.append("/>");
        }
        return sb.toString();
    }
}
