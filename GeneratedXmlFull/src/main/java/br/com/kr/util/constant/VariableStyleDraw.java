package br.com.kr.util.constant;

public enum VariableStyleDraw {
    //Constants color using style
    COLOR_BLACK_DEFAULT("#000000"),
    COLOR_BLUE_DEFAULT("#DAE8FC"),
    COLOR_GREEN_DEFAULT("#82B366"),
    COLOR_RED_DEFAULT("#F8CECC"),
    COLOR_WHITE_DEFAULT("#FFFFFF"),
    COLOR_YELLOW_DEFAULT("#FFF2CC"),

    //Constants color using style

    FONT_SIZE_BOTTOM_TEXT_STYLE_DEFAULT("12"),
    FONT_SIZE_BOTTOM_TEXT_VALUE_DEFAULT("14px"),
    FONT_SIZE_MIDDLE_TEXT_STYLE_DEFAULT("12"),
    FONT_SIZE_MIDDLE_TEXT_VALUE_DEFAULT("12px"),
    FONT_SIZE_TOP_TEXT_STYLE_DEFAULT("8"),
    FONT_SIZE_TOP_TEXT_VALUE_DEFAULT("8"),

    NOT_FOUND("");
    private final String valor;

    VariableStyleDraw(String valorOption) {
        valor = valorOption;
    }

    public String getValue() {
        return valor;
    }
}
