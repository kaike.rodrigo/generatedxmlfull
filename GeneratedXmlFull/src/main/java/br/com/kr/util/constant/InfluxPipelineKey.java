package br.com.kr.util.constant;

public enum InfluxPipelineKey {
    ADD("add"),
    AND("and"),
    ADD_DATA("addData"),
    AFTER("after"),
    ASYNC_NEXT("asyncNext"),
    BEFORE("before"),
    CODE("code"),
    COMPONENT("component"),
    CONFIG("config"),
    CONTEXT("context"),
    DATA("data"),
    DESCRIPTION("description"),
    DESTINATION("destination"),
    EQUAL("equal"),
    NOT_EQUAL("notEqual"),

    FINISH("finish"),
    JS_TRANSFORM("jsTransform"),
    MODE("mode"),
    MESSAGE_BUSINESS("messageBusiness"),
    MESSAGE_CODE("messageCode"),
    MESSAGE_DETAIL("messageDetail"),
    MESSAGE_TITLE("messageTitle"),
    MULTIPLE_NEXT("multipleNext"),
    NAME("name"),
    NEXT("next"),
    NEXT_STEP("nextStep"),
    OUTPUT_FIELD("outputField"),
    OUTPUT_FIELD_CODE("outputFieldCode"),
    PIPELINE_INCLUDE("pipeline-include"),
    SEQUENCE("sequence"),
    SEQUENCE_NEXT("sequenceNext"),
    STEP("step"),
    STEPS("steps"),
    SWITCH("switch"),
    SWITCH_DATA("switchData"),
    SWITCH_LIST("switchList"),
    TRY("try"),
    TYPE("type"),
    VERSION("version"),
    YET("yet"),
    PACK("PACK"),
    MIDDLE_TEXT("middleText"),
    BOTTOM_TEXT("bottomText"),
    BUNDLE("BUNDLE"),
    NOT_FOUND("");

    private final String value;
    InfluxPipelineKey(String valueOption){
        value = valueOption;
    }
    public String getValue(){
        return value;
    }
}
