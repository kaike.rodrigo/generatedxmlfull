package br.com.kr.util.constant;

public enum VariableDraw {
    GEOMETRY("geometry"),
    DIAMOND("diamond"),

    GEOMETRY_START_SCREEN_X_DEFAULT("-1510"),
    GEOMETRY_START_SCREEN_Y_DEFAULT("490"),
    GEOMETRY_START_SCREEN_CIRCLE_X_DEFAULT("490"),
    GEOMETRY_START_SCREEN_CIRCLE_Y_DEFAULT("80"),
    //    Values geometry default
    GEOMETRY_HEIGHT_DEFAULT("100"),
    GEOMETRY_WIDTH_DEFAULT("415"),
    GEOMETRY_X_DEFAULT("290"),
    GEOMETRY_Y_DEFAULT("250"),

    //    Values geometry draw circle default
    GEOMETRY_CIRCLE_HEIGHT_DEFAULT("60"),
    GEOMETRY_CIRCLE_WIDTH_DEFAULT("60"),
    GEOMETRY_CIRCLE_X_DEFAULT("1402"),
    GEOMETRY_CIRCLE_Y_DEFAULT("150"),

    //    Values geometry draw decision default
    GEOMETRY_DECISION_HEIGHT_DEFAULT("220"),
    GEOMETRY_DECISION_WIDTH_DEFAULT("415"),
    GEOMETRY_DECISION_X_DEFAULT("0"),
    GEOMETRY_DECISION_Y_DEFAULT("645"),

    //    Values geometry middle text
    GEOMETRY_HEIGHT_MIDDLE_TEXT_DEFAULT("30"),
    GEOMETRY_WIDTH_MIDDLE_TEXT_DEFAULT("320"),
    GEOMETRY_Y_MIDDLE_TEXT_DEFAULT("270"),

    //    Values geometry pack or bundle
    GEOMETRY_HEIGHT_PACK_OR_BUNDLE_DEFAULT("20"),
    GEOMETRY_WIDTH_PACK_OR_BUNDLE_DEFAULT("30"),
    GEOMETRY_Y_PACK_OR_BUNDLE_DEFAULT("250"),

    //    Values geometry bottom text
    GEOMETRY_HEIGHT_BOTTOM_TEXT_DEFAULT("30"),
    GEOMETRY_WIDTH_BOTTOM_TEXT_DEFAULT("320"),
    GEOMETRY_Y_BOTTOM_TEXT_DEFAULT("300"),

    //    Values geometry subtraction
    GEOMETRY_X_CALCULATION("290"),
    GEOMETRY_X_BACKGROUND_CALCULATION("210"),
    GEOMETRY_X_MIDDLE_TEXT_DEFAULT("290"),

    CIRCLE_DRAW("circleDraw"),
    DECISION_DRAW("decisionDraw"),
    BACKGROUND_DRAW("backgroundDraw"),
    CONNECTION_DRAW("connectionDraw"),
    CONNECTION_ASYNC_DRAW("connectionAsyncDraw"),
    SHAPE_BACKGROUND_DRAW("shapeBackgroundDraw"),

    NOT_FOUND("");
    private final String valor;

    VariableDraw(String valorOption) {
        valor = valorOption;
    }

    public String getValue() {
        return valor;
    }
}
