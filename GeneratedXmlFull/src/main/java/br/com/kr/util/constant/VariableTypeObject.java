package br.com.kr.util.constant;

public enum VariableTypeObject {
    BUNDLE("bundle"),
    DECISION("decision"),
    PACK("back"),
    PIPELINE("pipeline"),
    STEP("step"),
    STEPS("steps"),
    NOT_USED("");

    private final String value;
    VariableTypeObject(String valueOption){
        value = valueOption;
    }
    public String getValue(){
        return value;
    }
}
